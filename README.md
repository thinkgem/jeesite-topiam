# JeeSite 整合 TOPIAM 实现单点登录

## 简介

<p align="center">
<img src="https://jeesite.com/assets/images/logo-text.png" alt="logo" width="70%"/>
</p>

JeeSite，是一个 Java 快速开发平台，不仅仅是开发框架，它是一个企业级低代码解决方案，支持国产化，基于 Spring Boot 在线代码生成功能，采用经典开发模式。包括：组织角色用户、菜单按钮授权、数据权限、内容管理、工作流等。快速增减模块；微内核；安全选项丰富，密码策略；在线预览文件；消息推送；第三方登录；在线任务调度；支持集群、多租户、多数据源、读写分离、微服务。

<p align="center">
<img src="https://topiam.cn/img/full-logo.svg" alt="logo" width="70%"/>
</p>

[TOPIAM](https://topiam.cn)（Top Identity and Access Management），是一款开源的身份管理与访问控制系统，广泛应用于政府、企业内部、教育机构等身份认证场景。作为一款专注于身份管理与访问控制场景的软件产品，TOPIAM 支持 OIDC、OAuth2、SAML2、JWT、CAS 等主流认证协议，并能够集成钉钉、企业微信、飞书、LDAP、AD 等多种身份源，轻松实现用户全生命周期管理与数据同步。在认证方面，TOPIAM 支持用户名密码、短信/邮箱验证码等常规认证方式，并能集成钉钉、飞书、微信、企业微信、QQ 等社交平台登录，让用户能够通过常见平台便捷登录，从而显著提升用户体验。在安全性方面，TOPIAM 提供多因素认证、防暴力破解、会话管理、密码策略等能力，提升系统安全性。在审计方面，TOPIAM 提供全面的行为审计功能，详尽记录用户行为，发现潜在安全风险并及时采取防范措施，确保合规性和安全性。在信创方面，TOPIAM 全面支持从 CPU、操作系统、中间件、数据库、浏览器、国密算法的兼容适配，满足自主可控的技术需求。通过 TOPIAM，企业和团队能够快速实现统一的内外部身份认证，并集成各类应用，实现“一个账号、一次认证、多点通行”的效果，强化企业安全体系，提高组织管理效率，提升用户体验，助力企业数字化升级转型。

<p align="center">
<img src="./img/ui.png" alt="logo" width="2760"/>
</p>

+ 联系我们：http://s.jeesite.com 【有优惠】

## 集成步骤

### TOPIAM 配置

1. 新增OIDC应用

   ![](https://oscimg.oschina.net/oscnet/up-ab33c7221e44bb38ed5f7425a1829c0d92a.jpg)

   ![](https://oscimg.oschina.net/oscnet/up-45e0b17621178909d7130f5716db185d609.png)

2. 配置OIDC应用

+ 进入[应用配置]-[基本信息]，获取 客户端 ID 和 客户端秘钥。

   ![](https://oscimg.oschina.net/oscnet/up-4fc43f085fec36871bf7f49a50cd391fb6f.png)

+ 进入[应用配置]-[登录访问]-[单点登录]，配置 登录 Redirect URI 并更改授权范围为全员可访问，方便整合测试。

   ![](https://oscimg.oschina.net/oscnet/up-3dc6aabf56e2c13c521218ddeaecdfede1d.png)

+ 进入[应用配置]-[登录访问]-[单点登录]，在页面对下方展开[应用配置信息]获取授权端点地址。

   ![](https://oscimg.oschina.net/oscnet/up-7393a33fa76ecf864d14c94f7c77a2786e8.png)

### JeeSite 配置

1. 克隆本仓库源码，并添加到项目`modules`中，并在`modules`模块`pom.xml`中进行引入

    ![](https://oscimg.oschina.net/oscnet/up-e666fc7447c9d63fc168d628802946c9216.png)

    ```xml
    <modules>
       <module>core</module>
       <module>cms</module>
       <module>app</module>
       <!-- 引入topiam模块  -->
       <module>topiam</module>
    </modules>
    ```
2. 实现 `TopIamOauth2Service` 接口，并实现`getSysUserCode`方法，在模块实例中，我们内置了`MockTopIamOauth2ServiceImpl`实现类来模拟登录过程。

3. 在`web`模块配置`TopIamOauth2Service`接口实现类，为方便入门，你可以配置默认`MockTopIamOauth2ServiceImpl`实现查看效果。

    ```java
    import com.jeesite.modules.topiam.service.TopIamOauth2Service;
    
    /**
     * topiam 配置
     *
     * @author SanLi
     * Created by support@topiam.cn on  2023/8/30 21:52
     */
    @Configuration
    public class TopIamConfiguration {
    
       /**
        * TopIamOauth2Service
        *
        * @return {@link TopIamOauth2Service}
        */
       @Bean
       public TopIamOauth2Service topIamOauth2Service() {
          return new MockTopIamOauth2ServiceImpl();
       }
    }
    ```

4. 配置参数，在`web`模块`/src/main/resources/config`下找到`application.yml`文件，进行如下配置，并将${}内的内容改为TOPIAM配置步骤内获取的实际内容。

    ```properties
    oauth2:
      # TOPIAM
      topiam:
        clientId: ${client_id}
        clientSecret: ${client_secret}
        serverUrl: ${base_server_url}/api/v1/authorize/1xr4ngyqvd8qnze0oxbn2uquazbmxmb6
        redirectUri: ${base_redirect_uri}/js/oauth2/callback/topiam
        className: com.jeesite.modules.topiam.oauth.realm.request.AuthTopIamRequest
    ```

+ clientId：对应 TOPIAM 配置客户端ID
+ clientSecret：对应 TOPIAM 配置客户端秘钥
+ serverUrl：对应 TOPIAM 授权端点地址
+ redirectUri：对应 TOPIAM 登录 Redirect URI

## 测试

社区版集成，前端入口需要自己配置，为方便测试，大家可直接发起接口访问进行测试，此时用户登录将跳转TOPIAM门户端，登陆成功后回调到JeeSite完成登录。

接口地址：`https://127.0.0.1:8980/oauth2/login/topiam`

## 问题帮助

关注 JeeSite 微信公众号：

<img src="https://jeesite.com/assets/images/mp.png" alt="logo" width="30%"/>

如果您在集成中出现任何问题，可通过微信群进行反馈。

<img src="./img/wechat-group.png" alt="logo" width="30%"/>
