package com.jeesite.modules.topiam.service;

import me.zhyd.oauth.model.AuthUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 默认实现
 *
 * @author SanLi
 * Created by support@topiam.cn on  2023/8/30 21:24
 */
public class MockTopIamOauth2ServiceImpl implements TopIamOauth2Service {

    private final Logger logger = LoggerFactory.getLogger(MockTopIamOauth2ServiceImpl.class);

    /**
     * 换取系统用户ID
     *
     * @param authUser {@link AuthUser}
     * @return {@link Boolean}
     */
    @Override
    public String getSysUserCode(AuthUser authUser) {
        return DEFAULT_USER_CODE;
    }


    private static final String DEFAULT_USER_CODE = "system";
}
