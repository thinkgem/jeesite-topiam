package com.jeesite.modules.topiam.config;

import com.jeesite.modules.topiam.service.MockTopIamOauth2ServiceImpl;
import com.jeesite.modules.topiam.service.TopIamOauth2Service;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * topiam 配置
 *
 * @author SanLi
 * Created by support@topiam.cn on  2023/8/30 21:52
 */
@Configuration
@EnableConfigurationProperties(TopIamProperties.class)
public class TopIamConfiguration {

    /**
	* TopIamOauth2Service
	*
	* @return {@link TopIamOauth2Service}
	*/
   @Bean
   public TopIamOauth2Service topIamOauth2Service() {
	  return new MockTopIamOauth2ServiceImpl();
   }

}