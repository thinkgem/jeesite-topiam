package com.jeesite.modules.topiam.oauth;

import com.alibaba.fastjson.JSONObject;
import com.beust.jcommander.internal.Lists;
import com.jeesite.modules.topiam.config.TopIamProperties;
import com.xkcoding.http.HttpUtil;
import com.xkcoding.http.config.HttpConfig;
import com.xkcoding.http.support.AbstractHttp;
import com.xkcoding.http.support.HttpHeader;
import com.xkcoding.http.support.java11.HttpClientImpl;
import com.xkcoding.http.support.okhttp3.OkHttp3Impl;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.config.AuthSource;
import me.zhyd.oauth.exception.AuthException;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthToken;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthDefaultRequest;
import me.zhyd.oauth.utils.Base64Utils;
import me.zhyd.oauth.utils.UrlBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * TOPIAM 认证请求
 *
 * @author SanLi
 * Created by support@topiam.cn on  2023/8/29
 */
public class AuthTopIamRequest extends AuthDefaultRequest {
    private final Logger logger = LoggerFactory.getLogger(AuthTopIamRequest.class);

    public AuthTopIamRequest(TopIamProperties topIamProperties) {
        super(AuthConfig.builder()
                .clientId(topIamProperties.getClientId())
                .clientSecret(topIamProperties.getClientSecret())
                .redirectUri(topIamProperties.getClientRedirectUri())
                .scopes(Lists.newArrayList("openid", "profile"))
                .build(), new AuthSource() {
            @Override
            public String authorize() {
                return topIamProperties.getAuthorizeEndpoint();
            }
            @Override
            public String accessToken() {
                return topIamProperties.getTokenEndpoint();
            }

            @Override
            public String userInfo() {
                return topIamProperties.getUserinfoEndpoint();
            }

            @Override
            public Class<? extends AuthDefaultRequest> getTargetClass() {
                return AuthTopIamRequest.class;
            }
        });
    }

    @Override
    protected AuthToken getAccessToken(AuthCallback authCallback) {
        logger.debug("Get TOPIAM access_token request :{}", JSONObject.toJSONString(authCallback));
        String body = getAccessToken(authCallback.getCode());
        JSONObject response = JSONObject.parseObject(body);
        logger.debug("Get TOPIAM access_token response :{}", JSONObject.toJSONString(response));
        checkResponse(response);
        //@formatter:off
        return AuthToken.builder()
                .accessToken(response.getString("access_token"))
                .refreshToken(response.getString("refresh_token"))
                .idToken(response.getString("id_token"))
                .tokenType(response.getString("token_type"))
                .scope(response.getString("scope"))
                .build();
        //@formatter:on
    }

    @Override
    protected AuthUser getUserInfo(AuthToken authToken) {
        logger.debug("Get TOPIAM user info request :{}", JSONObject.toJSONString(authToken));
        String body = doGetUserInfo(authToken);
        JSONObject result = JSONObject.parseObject(body);
        logger.debug("Get TOPIAM user info response :{}", JSONObject.toJSONString(authToken));
        checkResponse(result);
        //@formatter:off
        return AuthUser.builder()
                .uuid(result.getString("sub"))
                .username(result.getString("preferred_username"))
                .nickname(result.getString("nickname"))
                .avatar(result.getString("picture"))
                .email(result.getString("email"))
                .token(authToken)
                .source(source.toString())
                .build();
        //@formatter:on
    }

    @Override
    protected String doGetUserInfo(AuthToken authToken) {
        HttpUtil.setHttp(getHttp());
        return HttpUtil
                .get(source.userInfo(), null, new HttpHeader().add("Content-Type", "application/json")
                        .add("Authorization", "Bearer " + authToken.getAccessToken()), false)
                .getBody();
    }


    @Override
    public String authorize(String state) {
        return UrlBuilder.fromBaseUrl(super.authorize(state))
                .queryParam("scope", StringUtils.join(this.config.getScopes(), " "))
                .build();
    }

    public static void checkResponse(JSONObject object) {
        // oauth/token 验证异常
        if (object.containsKey("error")) {
            throw new AuthException(object.getString("error_description"));
        }
        // user 验证异常
        if (object.containsKey("message")) {
            throw new AuthException(object.getString("message"));
        }
    }

    protected String getAccessToken(String code) {
        HttpHeader httpHeader = new HttpHeader();
        httpHeader.add("Authorization", getBasic(config.getClientId(), config.getClientSecret()));
        Map<String, String> form = new HashMap<>(7);
        form.put("code", code);
        form.put("grant_type", "authorization_code");
        form.put("redirect_uri", config.getRedirectUri());
        HttpUtil.setHttp(getHttp());
        return HttpUtil.post(source.accessToken(), form, httpHeader, false).getBody();
    }

    private String getBasic(String appKey, String appSecret) {
        StringBuilder sb = new StringBuilder();
        String encodeToString = Base64Utils.encode((appKey + ":" + appSecret).getBytes());
        sb.append("Basic").append(" ").append(encodeToString);
        return sb.toString();
    }

    private AbstractHttp getHttp() {
        return new OkHttp3Impl();
    }

}
