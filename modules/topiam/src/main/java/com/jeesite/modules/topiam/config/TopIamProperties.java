package com.jeesite.modules.topiam.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 * @author SanLi
 * Created by qinggang.zuo@gmail.com / 2689170096@qq.com on  2024/2/12 14:20
 */
@Data
@ConfigurationProperties(prefix = "oauth2.topiam")
public class TopIamProperties {

    /**
     * client id
     */
    private String clientId;

    /**
     * client secret
     */
    private String clientSecret;

    /**
     * 授权端点
     */
    private String authorizeEndpoint;

    /**
     * token 端点
     */
    private String tokenEndpoint;

    /**
     * 用户信息断点
     */
    private String userinfoEndpoint;

    /**
     * client redirect uri
     */
    private String clientRedirectUri;
}
