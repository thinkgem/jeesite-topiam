package com.jeesite.modules.topiam.web;

import com.beust.jcommander.internal.Lists;
import com.jeesite.common.mapper.JsonMapper;
import com.jeesite.common.shiro.authc.FormToken;
import com.jeesite.common.shiro.filter.FormFilter;
import com.jeesite.common.web.BaseController;
import com.jeesite.common.web.http.ServletUtils;
import com.jeesite.modules.sys.utils.UserUtils;
import com.jeesite.modules.topiam.config.TopIamProperties;
import com.jeesite.modules.topiam.oauth.AuthTopIamRequest;
import com.jeesite.modules.topiam.service.TopIamOauth2Service;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * TOPIAM OAuth2 Controller
 *
 * @author TOPIAM
 * Created by support@topiam.cn on  2023/08/30 13:11
 */
@Controller
@RequestMapping({"/oauth2"})
public class TopIamOauth2Controller extends BaseController {

    /**
     * topiam login
     *
     * @param request {@link HttpServletRequest}
     * @return {@link String}
     */
    @RequestMapping({"/login/topiam"})
    public String login(HttpServletRequest request) {
        logger.debug("Redirect topiam authentication");
        return "redirect:" + authTopIamRequest.authorize((request.getParameter("state") == null ? AuthStateUtils.createState() : request.getParameter("state")));
    }

    /**
     * TOPIAM callback
     *
     * @param callback {@link AuthCallback}
     */
    @RequestMapping({"/callback/topiam"})
    public void callback(AuthCallback callback) {
        logger.debug("Reception topiam callback");
        AuthResponse<?> rauthResponse = authTopIamRequest.login(callback);
        AuthUser authUser = (AuthUser) rauthResponse.getData();
        logger.debug("Get topiam result: {}", JsonMapper.toJson(authUser));
        HttpServletRequest request = ServletUtils.getRequest();
        HttpServletResponse response = ServletUtils.getResponse();
        try {
            // FormToken 构造方法的三个参数：登录名、是否内部登录无条件、请求对象
            UserUtils.getSubject().login(new FormToken(topIamOauth2Service.getSysUserCode(authUser), true, request));
            logger.debug("Authentication success, __sid={}", UserUtils.getSession().getId());
            FormFilter.onLoginSuccess(request, response);
        } catch (AuthenticationException e) {
            FormFilter.onLoginFailure(e, request, response);
        }
    }


    private final TopIamOauth2Service topIamOauth2Service;

    private final AuthTopIamRequest authTopIamRequest;

    public TopIamOauth2Controller(TopIamOauth2Service topIamOauth2Service, TopIamProperties topIamProperties) {
        this.topIamOauth2Service = topIamOauth2Service;
        this.authTopIamRequest =  new AuthTopIamRequest(topIamProperties);
    }
}
