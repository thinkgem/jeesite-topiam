package com.jeesite.modules.topiam.service;

import me.zhyd.oauth.model.AuthUser;

/**
 * @author SanLi
 * Created by support@topiam.cn on  2023/8/30 21:23
 */
public interface TopIamOauth2Service {

    /**
     * 换取系统用户ID
     *
     * @param authUser {@link AuthUser}
     * @return {@link Boolean}
     */
    String getSysUserCode(AuthUser authUser);
}
