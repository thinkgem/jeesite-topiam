# TOPIAM 服务端

## 开源地址

+ Gitee 地址：https://gitee.com/topiam/eiam
+ GitHub 地址：https://github.com/topiam/eiam
+ GitCode 地址：https://gitcode.com/topiam/eiam


## 在线安装

安装前请确保您的系统符合安装条件：

  + 操作系统：支持主流 Linux 发行版本（基于 Debian / RedHat）；
  + 服务器架构：x86_64、aarch64；
  + 内存要求：建议可用内存在 8GB 以上；
  + 浏览器要求：请使用 Chrome、FireFox、Edge 等现代浏览器；
  + 端口要求：22（安装、升级及管理使用）、1898（默认 Web 服务访问端口，根据实际情况进行更改）；
  + 可访问互联网；

在服务器上执行一键安装命令（该命令适合于 CentOS、Ubuntu、Debian 系统）：

```shell
curl -sSL https://resource.topiam.cn/quick_start.sh -o quick_start.sh && bash quick_start.sh
```

